class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        return self.slowVersion(nums)

    def slowVersion(self, nums):
        isOne = False
        counter = 0
        max = 0
        for num in nums:
            if num == 1:
                if isOne == False:
                    isOne = True
                counter += 1
            else:
                if isOne == True:
                    if counter > max:
                        max = counter
                    counter = 0
                    isOne = False
        if counter > max:
            max = counter
        return max

s = Solution()
m = s.findMaxConsecutiveOnes([1,0,1,1,0,1])
print(m)