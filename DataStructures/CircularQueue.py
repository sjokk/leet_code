# Circular queue is empty if head and tail are in the same position
# and that location holds no data.

# Circular queue is full if the difference in position between tail
# and head is 1 position.

class MyCircularQueue(object):

    def __init__(self, k):
        """
        Initialize your data structure here. Set the size of the queue to be k.
        :type k: int
        """
        self.queue = [None for i in range(k)]
        self.queueSize = k
        self.front = self.rear = None

    def enQueue(self, value):
        """
        Insert an element into the circular queue. Return true if the operation is successful.
        :type value: int
        :rtype: bool
        """
        if self.isFull() == False:
            if self.front is None:
                self.front = self.rear = 0
            else:
                self.rear = (self.rear + 1) % self.queueSize
            self.queue[self.rear] = value
            return True
        return False

    def deQueue(self):
        """
        Delete an element from the circular queue. Return true if the operation is successful.
        :rtype: bool
        """
        if self.isEmpty() == False:
            self.queue[self.front] = None
            self.front = (self.front + 1) % self.queueSize
            return True
        return False

    def Front(self):
        """
        Get the front item from the queue.
        :rtype: int
        """
        if self.isEmpty():
            return -1
        else:
            return self.queue[self.front]

    def Rear(self):
        """
        Get the last item from the queue.
        :rtype: int
        """
        if self.isEmpty():
            return -1
        else:
            return self.queue[self.rear]

    def isEmpty(self):
        """
        Checks whether the circular queue is empty or not.
        :rtype: bool
        """
        # Circular queue is empty if both pointers are not set (after instantiation)
        return (self.front is None and self.rear is None) or (self.front == self.rear and self.queue[self.front] is None) or self.queue[self.front] == None
        

    def isFull(self):
        """
        Checks whether the circular queue is full or not.
        :rtype: bool
        """
        # Circular queue is full if it is not empty and if the pointers of front and rear are one apart or if rear + 1 % size is at the same position as front.
        return self.isEmpty() == False and (self.rear + 1) % self.queueSize == self.front

# q = MyCircularQueue(1)
# print(q.enQueue(1))
# print(q.enQueue(2))
# print(q.deQueue())
# print(q.deQueue())
# print(q.isEmpty())
# print(q.enQueue(2))
# print(q.Rear())
# print(q.Front())
# print(q.deQueue())
# print(q.enQueue(None))
# print(q.Front())

circularQueue = MyCircularQueue(6)
print(circularQueue.enQueue(6))
print(circularQueue.Rear())
print(circularQueue.Rear())
print(circularQueue.deQueue())
print(circularQueue.enQueue(5))
print(circularQueue.Rear())
print(circularQueue.deQueue())
print(circularQueue.Front())
print(circularQueue.deQueue())
print(circularQueue.deQueue())
print(circularQueue.deQueue())

# Your MyCircularQueue object will be instantiated and called as such:
# obj = MyCircularQueue(k)
# param_1 = obj.enQueue(value)
# param_2 = obj.deQueue()
# param_3 = obj.Front()
# param_4 = obj.Rear()
# param_5 = obj.isEmpty()
# param_6 = obj.isFull()