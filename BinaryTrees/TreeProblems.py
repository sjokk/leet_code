from TreeNode import TreeNode

class Solution(object):
    def maxDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        # For as long as left and right are not both none.
        if root is None:
            return 0
        elif root.left is None and root.right is None:
            return 1
        else:
            # Go into left and right
            left_depth = 1 + self.maxDepth(root.left)
            right_depth = 1 + self.maxDepth(root.right)
            # Return the maximum depth between left and right.
            return max(left_depth, right_depth)
            
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        # If both children are None, it is symmetric
        # The treeNode seems to be symmetric if either left and right child are not None and have an equal value.
        pass 
        # If this is not a leaf node and one of the children
        # is None, it is not symmetric

    def hasPathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: bool
        """
        if root is not None:
            # Only continue if sum - TreeNode.val >= 0 else return up the stack
            if sum - root.val == 0 and root.left is None and root.right is None:
                return True
            elif sum - root.val > 0:
                # Continue exploring the children
                return self.hasPathSum(root.left, sum - root.val) or self.hasPathSum(root.right, sum - root.val)


s = Solution()

# t = TreeNode(5)
# t.left = TreeNode(4)
# t.left.left = TreeNode(11)
# t.left.left.left = TreeNode(7)
# t.left.left.right = TreeNode(2)

# t.right = TreeNode(8)
# t.right.left = TreeNode(13)
# t.right.right = TreeNode(4)
# t.right.right.right = TreeNode(1)

# print(s.hasPathSum(t, 22))

# # t1 is symmetric
# t1 = TreeNode(1)
# t1.left = TreeNode(2)
# t1.right = TreeNode(2)
# t1.left.left = TreeNode(3)
# t1.left.right = TreeNode(4)
# t1.right.left = TreeNode(4)
# t1.right.right = TreeNode(3)

# # t2 is not symmetric
# t2 = TreeNode(1)
# t2.left = TreeNode(2)
# t2.right = TreeNode(2)
# t2.left.right = TreeNode(3)
# t2.right.right = TreeNode(3)

# print(s.isSymmetric(t1))
# print(s.isSymmetric(t2))

# t = None
# print(s.maxDepth(t))

# t = TreeNode(3)

# print(s.maxDepth(t))

# t = TreeNode(3)
# t.left = TreeNode(9)
# t.right = TreeNode(20)
# t.right.left = TreeNode(15)
# t.right.right = TreeNode(7)

# print(s.maxDepth(t))