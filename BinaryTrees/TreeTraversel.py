from TreeNode import TreeNode

class Solution(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        rtype: List[int]
        """
        result = []
        # Add the root node first, then iterate
        # the left sub tree and after that right.
        if root is not None:
            result = self.preorderTraversalHelper(root)
        return result

    def preorderTraversalHelper(self, treeNode):
        result = []
        if treeNode is not None:
            # Add the root of the sub tree first.
            result.append(treeNode.val)
            # Try to obtain the left subtree's subtree
            result += self.preorderTraversalHelper(treeNode.left)
            # Same for the right subtree.
            result += self.preorderTraversalHelper(treeNode.right)
        return result

    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        result = []
        if root is not None:
            result = self.inorderTraversalHelper(root)
        return result

    def inorderTraversalHelper(self, treeNode):
        result = []
        if treeNode is not None:
            # Try to obtain the left subtree's subtree first
            result += self.inorderTraversalHelper(treeNode.left)
            # Then add the current node to the result list
            result.append(treeNode.val)
            # Try to obtain the left subtree's subtree first
            result += self.inorderTraversalHelper(treeNode.right)
        return result

    def postorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        result = []
        if root is not None:
            result = self.postorderTraversalHelper(root)
        return result

    def postorderTraversalHelper(self, treeNode):
        result = []
        if treeNode is not None:
            # Try to obtain the left subtree's subtree first
            result += self.postorderTraversalHelper(treeNode.left)
            # Try to obtain the left subtree's subtree first
            result += self.postorderTraversalHelper(treeNode.right)
            # Then add the current node to the result list
            result.append(treeNode.val)
        return result

    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        queue = []
        result = []
        if root is not None:
            queue.append([root])
            result.append([root.val])
        
        # Every sub list represents another level which we need to examine as a whole.
        for level in queue:
            # A new sub list representing the next level.
            nextLevel = []
            levelResult = []
            # For every child we need to obtain its children to rep the next level.
            for node in level:
                if node.left is not None:
                    nextLevel.append(node.left)
                    levelResult.append(node.left.val)
                if node.right is not None:
                    nextLevel.append(node.right)
                    levelResult.append(node.right.val)
            if (nextLevel != []):
                result.append(levelResult)
                queue.append(nextLevel)
        return result


s = Solution()
root = TreeNode(3)
root.left = TreeNode(9)
root.right = TreeNode(20)
root.right.left = TreeNode(15)
root.right.right = TreeNode(7)

result = s.levelOrder(root)
print(result)