# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        

l1 = ListNode(5)       
l1.add(ListNode(4))
l1.add(ListNode(3))

print(l1)

l2 = ListNode(5)
l2.add(ListNode(6))
l2.add(ListNode(4))

print(l2)

s = Solution()
print(s.addTwoNumbers(l1, l2))