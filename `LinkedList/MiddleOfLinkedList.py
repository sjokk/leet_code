# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def middleNode(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        nodes = []
        current = head
        nodeCounter = 0
        while current != None:
            # Add it to the list to return in constant time
            # after we counted all nodes.
            nodes.append(current)
            nodeCounter += 1
            current = current.next
        return nodes[nodeCounter // 2]

s = Solution()
r1 = ListNode(1)
r1.next = ListNode(2)
r1.next.next = ListNode(3)
r1.next.next.next = ListNode(4)
r1.next.next.next.next = ListNode(5)

print(f"{s.middleNode(r1).val}")

r2 = ListNode(1)
r2.next = ListNode(2)
r2.next.next = ListNode(3)
r2.next.next.next = ListNode(4)
r2.next.next.next.next = ListNode(5)
r2.next.next.next.next.next = ListNode(6)

print(f"{s.middleNode(r2).val}")