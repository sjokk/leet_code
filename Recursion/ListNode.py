# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    # Recursive add method, adds node in next if next is none,
    # otherwise it calls the next node's add method until it finds
    # a spot to add the input node.
    def add(self, node):
        if self.next is None:
            self.next = node
        else:
            self.next.add(node)
    
    def __str__(self):
        result = f"{self.val}"
        if self.next is not None:
            result += f"->{self.next}"
        return result
