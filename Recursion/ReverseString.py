class Solution(object):
    def reverseString(self, s):
        """
        :type s: List[str]
        :rtype: None Do not return anything, modify s in-place instead.
        """
        return self.recursiveSwap(s, 0, len(s) - 1)

    def recursiveSwap(self, s, start, end):
        # The array is empty?
        if s == []:
            return s
        # Start and end index are equal since we were dealing with an odd number of characters array.
        elif start == end:
            return s
        # Start and end converged, meaning we dealt with an even number of characters array.
        elif start > end:
            return s
        else:
            # Swap character and make recursive call.
            s[start], s[end] = s[end], s[start]
            return self.recursiveSwap(s, start + 1, end - 1)
        

s = Solution()

s1 = ["h","e","l","l","o"]
s2 = ["H","a","n","n","a","h"]

print(s.reverseString(s1))
print(s.reverseString(s2))