from ListNode import ListNode

class Solution(object):
    def swapPairs(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        # Swapping pairs results in node.val = node.next.val
        # and node.next.val = node.val, since these are integers
        # we could also do this in a mathematical manner as other
        # programming languages don't allow what Python allows.
        # 
        # node.val = node.val + node.next.val
        # node.next.val = node.val - node.next.val (resulting in node.val's original value).
        # node.val = node.val - node.next.val (resulting in node.next.val's original value).
        if head is None:
            return
        elif head.next is None:
            return head
        else:
            # Swap position of head and adjacent node and make a recursive call.
            head.val = head.val + head.next.val
            head.next.val = head.val - head.next.val
            head.val = head.val - head.next.val
            self.swapPairs(head.next.next)
        return head

sll = ListNode(1)
sll.add(ListNode(2))
sll.add(ListNode(3))
sll.add(ListNode(4))

print(sll)

s = Solution()
s.swapPairs(sll)

print(sll)
